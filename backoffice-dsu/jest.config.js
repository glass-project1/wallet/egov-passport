module.exports = {
  verbose: true,
  transform: {'^.+\\.ts?$': 'ts-jest'},
  testEnvironment: 'node',
  testRegex: './backoffice-dsu/tests/.*\\.(test|spec)\\.(ts)$',
  // globalSetup: "./node_modules/@glass-project1/dsu-utils/src/jest-setup.js",
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
  moduleNameMapper: {
    "^\\./db-decorators.js$": "<rootDir>/node_modules/@glass-project1/db-decorators/dist/db_decorators.bundle.js",
    "^\\./db-decorators.esm.js$": "<rootDir>/node_modules/@glass-project1/db-decorators/dist/esm/db_decorators.bundle.esm.js",
    "^\\./decorator-validation.js$": "<rootDir>/node_modules/@glass-project1/decorator-validation/dist/decorator_validation.bundle.js",
    "^\\./decorator-validation.esm.js$": "<rootDir>/node_modules/@glass-project1/decorator-validation/dist/esm/decorator_validation.bundle.esm.js",
    "^\\./opendsu-types.js$": "<rootDir>/node_modules/@glass-project1/opendsu-types/dist/opendsu_types.bundle.js",
    "^\\./opendsu-types.esm.js$": "<rootDir>/node_modules/@glass-project1/opendsu-types/dist/esm/opendsu_types.bundle.esm.js",
    "^\\./logging.js$": "<rootDir>/node_modules/@glass-project1/logging/dist/logging.bundle.js",
    "^\\./logging.esm.js$": "<rootDir>/node_modules/@glass-project1/logging/dist/esm/logging.bundle.esm.js"
  },
  // moduleDirectories: [
  //   "node_modules",
  //   __dirname,
  // ],
  // transformIgnorePatterns: [
  //   "node_modules/(?!(@glass-project1)/opendsu-types)",
  // ],
  collectCoverage: true,
  collectCoverageFrom: ['src/**/*.{ts,jsx}'],
  coverageDirectory: "./workdocs/coverage",
  coverageReporters: [
    "json-summary",
    "text-summary",
    "text",
    "html"
  ],
  reporters: [
    "default",
    ["jest-junit", {outputDirectory: './workdocs/coverage', outputName: "junit-report.xml"}]
  ]
};