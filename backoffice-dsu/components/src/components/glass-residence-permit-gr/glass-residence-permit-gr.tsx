import { Component, Host, h, Prop } from '@stencil/core';

@Component({
  tag: 'glass-residence-permit-gr',
  styleUrl: 'glass-residence-permit-gr.scss',
  shadow: true,
})
export class GlassResidencePermitGr {

  /**
   * The surname of the residence permit
   */
  @Prop({attribute: "surname"}) surname: string = "dolor sit amet";
  /**
    * The givenname of the residence permit
    */
  @Prop({attribute: "given-name"}) givenName: string = "Lorem ipsum";

  /**
   * The expiration of the residence permit
   */
  @Prop({attribute: "expiration-date"}) expiryDate: string = "01 01 2030";

  /**
   * The issuing date of the residence permit
   */
  @Prop({attribute: "issuing-date"}) issuingDate: string = "01 01 2030";

  /**
   * The issuing authority of the residence permit
   */
  @Prop({attribute: "issuing-authority"}) issuingAuthority: string = "Lorem";

  /**
   * The permit duration of the residence permit
   */
  @Prop({attribute: "permit-duration"}) permitDuration : string = "permanent";
  /**
   * The issuing date of the residence permit
   */
  @Prop({attribute: "permit-type"}) permitType: string = "residence";



  render() {
    const self = this;

    return (
        <div class="g-bank">
          <ion-grid>
            <ion-row>
              <ion-col class="g-bank-header">
                <div class="g-bank-icon">
                  <ion-icon name="checkmark-outline"></ion-icon>
                </div>
                <ion-text>
                  <h1>άδεια παραμονής</h1>
                </ion-text>
              </ion-col>
            </ion-row>
            <ion-row>
              <ion-col class="g-bank-content">
                <ion-list lines="none">

                  <ion-item>
                    <ion-icon name="reader-outline" slot="start"></ion-icon>
                    <ion-label>
                      <h2>Πελάτης</h2>
                      <p>{self.givenName}</p>
                    </ion-label>
                  </ion-item>

                  <ion-item>
                    <ion-icon name="reader-outline" slot="start"></ion-icon>
                    <ion-label>
                      <h2>Διάρκεια</h2>
                      <p>{self.permitDuration}</p>
                    </ion-label>
                  </ion-item>

                  <ion-item>
                    <ion-icon name="reader-outline" slot="start"></ion-icon>
                    <ion-label>
                      <h2>Τύπος</h2>
                      <p>{self.permitType}</p>
                    </ion-label>
                  </ion-item>

                </ion-list>
              </ion-col>
            </ion-row>
          </ion-grid>
        </div>
    );
  }


}
