import { newE2EPage } from '@stencil/core/testing';

describe('glass-residence-permit-gr', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-residence-permit-gr></glass-residence-permit-gr>');

    const element = await page.find('glass-residence-permit-gr');
    expect(element).toHaveClass('hydrated');
  });
});
