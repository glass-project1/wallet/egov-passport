import { newE2EPage } from '@stencil/core/testing';

describe('glass-residence-permit-pt', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-residence-permit-pt></glass-residence-permit-pt>');

    const element = await page.find('glass-residence-permit-pt');
    expect(element).toHaveClass('hydrated');
  });
});
