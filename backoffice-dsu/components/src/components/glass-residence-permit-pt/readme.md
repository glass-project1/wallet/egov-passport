# glass-residence-permit-pt



<!-- Auto Generated Below -->


## Properties

| Property           | Attribute           | Description                                   | Type     | Default            |
| ------------------ | ------------------- | --------------------------------------------- | -------- | ------------------ |
| `expiryDate`       | `expiration-date`   | The expiration of the residence permit        | `string` | `"01 01 2030"`     |
| `givenName`        | `given-name`        | The givenname of the residence permit         | `string` | `"Lorem ipsum"`    |
| `issuingAuthority` | `issuing-authority` | The issuing authority of the residence permit | `string` | `"Lorem"`          |
| `issuingDate`      | `issuing-date`      | The issuing date of the residence permit      | `string` | `"01 01 2030"`     |
| `permitDuration`   | `permit-duration`   | The permit duration of the residence permit   | `string` | `"permanent"`      |
| `permitType`       | `permit-type`       | The issuing date of the residence permit      | `string` | `"residence"`      |
| `surname`          | `surname`           | The surname of the residence permit           | `string` | `"dolor sit amet"` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
