import { newE2EPage } from '@stencil/core/testing';

describe('glass-residence-permit-tk', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-residence-permit-tk></glass-residence-permit-tk>');

    const element = await page.find('glass-residence-permit-tk');
    expect(element).toHaveClass('hydrated');
  });
});
