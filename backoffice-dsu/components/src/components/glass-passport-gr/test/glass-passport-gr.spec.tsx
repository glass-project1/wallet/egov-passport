import { newSpecPage } from '@stencil/core/testing';
import { GlassPassportGr } from '../glass-passport-gr';

describe('glass-passport-gr', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [GlassPassportGr],
      html: `<glass-passport-gr></glass-passport-gr>`,
    });
    expect(page.root).toEqualHtml(`
    <glass-passport-gr>
      <mock:shadow-root>
      <ion-card class="g-passport">
      <ion-card-header>
        <img alt="" src="/assets/images/republic-icon.svg">
        <ion-text>
          <ion-card-title>República Portuguesa</ion-card-title>
          <ion-card-subtitle>Portuguese Republic</ion-card-subtitle>
        </ion-text>
      </ion-card-header>
      <ion-card-content>

        <ion-grid class="g-passport__content-top">
          <ion-row>
            <ion-col class="g-passport__profile" size="4">
              <img alt="" src="/assets/images/profile-male.svg">
            </ion-col>
            <ion-col size='8'>
              <ion-row class="g-passport__content-top-right">
                <ion-col size='8'>
                  <ion-text>
                    <h2>Passaport</h2>
                    <h4>Passport</h4>
                  </ion-text>
                </ion-col>
                <ion-col size='4'>
                  <img alt="" class="g-passport__icon" src="/assets/images/passport-icon.svg">
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col size='2'>
                  <ion-text>
                    <h2>Tipo</h2>
                    <h4>Type</h4>
                    <p>PC</p>
                  </ion-text>
                </ion-col>
                <ion-col size='5'>
                  <ion-text>
                    <h2>Passaporte nº</h2>
                    <h4>Passaport no.</h4>
                    <p>AA98765</p>
                  </ion-text>
                </ion-col>
                <ion-col size='5'>
                  <ion-text>
                    <h2>Cód. do país</h2>
                    <h4>Code</h4>
                    <p>PRT</p>
                  </ion-text>
                </ion-col>
              </ion-row>
            </ion-col>
          </ion-row>
        </ion-grid>

        <ion-grid class="g-passport__content-body">
          <ion-row>
            <ion-col size='6'>
              <ion-label>1.</ion-label>
              <ion-text>
                <h2>Apelido</h2>
                <h4>Surname</h4>
                <p>Santos Barbosa</p>
              </ion-text>
            </ion-col>
            <ion-col size='6'>
              <ion-label>2.</ion-label>
              <ion-text>
                <h2>Nomes próprios</h2>
                <h4>Given names</h4>
                <p>Jorge Cardoso</p>
                </ion-text>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col size='6'>
              <ion-label>3.</ion-label>
              <ion-text>
                <h2>Nacionalidade</h2>
                <h4>Nationality</h4>
                <p>Portuguesa</p>
              </ion-text>
            </ion-col>
            <ion-col size='6'>
              <ion-label>4.</ion-label>
              <ion-text>
                <h2>Altura</h2>
                <h4>Height</h4>
                <p>1.72 m</p>
              </ion-text>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col size='6'>
              <ion-label>5.</ion-label>
              <ion-text>
                <h2>Data de nascimento</h2>
                <h4>Date of birth</h4>
                <p>07.04.1974</p>
              </ion-text>
            </ion-col>
            <ion-col size='6'>
            <ion-label>6.</ion-label>
              <ion-text>
                <h2>Nº de ident. pessoal</h2>
                <h4>Personal no.</h4>
                <p>90876549</p>
              </ion-text>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col size='6'>
            <ion-label>7.</ion-label>
              <ion-text>
                <h2>Sexo</h2>
                <h4>Sex</h4>
                <p>M</p>
              </ion-text>
            </ion-col>
            <ion-col size='6'>
            <ion-label>8.</ion-label>
              <ion-text>
                <h2>Local de nascimento</h2>
                <h4>Place of birth</h4>
                <p>Mação*Santarém</p>
              </ion-text>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col size='6'>
            <ion-label>9.</ion-label>
              <ion-text>
                <h2>Data da emissão</h2>
                <h4>Date of issue</h4>
                <p>16.02.2020</p>
              </ion-text>
            </ion-col>
            <ion-col size='6'>
            <ion-label>10.</ion-label>
              <ion-text>
                <h2>Autoridade</h2>
                <h4>Authority</h4>
                <p>SEF - Serv Estr e Fronteiras</p>
              </ion-text>
            </ion-col>
          </ion-row>
          <ion-row>
            <ion-col size='6'>
            <ion-label>11.</ion-label>
              <ion-text>
                <h2>Válido até</h2>
                <h4>Date of expiry</h4>
                <p>22.06.2028</p>
              </ion-text>
            </ion-col>
            <ion-col size='6'>
            <ion-label>12.</ion-label>
              <ion-text>
                <h2>Assinatura do titular</h2>
                <h4>Holder's signature</h4>
                <img alt="" src="/assets/images/signature-id.png">
              </ion-text>
            </ion-col>
          </ion-row>
        </ion-grid>

        <ion-grid class="g-passport__content-bottom">
          <ion-row>
            <ion-col size='12'><p>P&lt;prtsantos&lt;barbosa&lt;ana&lt;&lt;cardoso&lt;&lt;&lt;</p></ion-col>
            <ion-col size='12'><p>AA98765&lt;0PRT9087654904030&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;81</p></ion-col>
          </ion-row>
        </ion-grid>

      </ion-card-content>
    </ion-card>
      </mock:shadow-root>
    </glass-passport-gr>
  `);
  });
});
