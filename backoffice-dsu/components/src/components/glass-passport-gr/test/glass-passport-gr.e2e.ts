import { newE2EPage } from '@stencil/core/testing';

describe('glass-passport-gr', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-passport-gr></glass-passport-gr>');

    const element = await page.find('glass-passport-gr');
    expect(element).toHaveClass('hydrated');
  });
});
