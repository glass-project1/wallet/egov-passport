import { Component, Host, h, Prop, getAssetPath } from '@stencil/core';

@Component({
  tag: 'glass-passport',
  styleUrl: 'glass-passport.scss',
  shadow: true,
})
export class GlassPassport {

  /**
   * The location of the republic source icon
   */
  @Prop({attribute: "src-republic"}) srcRepublic: string = "republic-icon.svg";

  /**
   * The location of the passport source icon
   */
  @Prop({attribute: "src-passport"}) srcPassport: string = "passport-icon.svg";

  /**
   * The location of the profile male icon
   */
  @Prop({attribute: "src-male"}) srcMale: string = "profile-male.svg";

  /**
   * The location of the profile male icon
   */
   @Prop({attribute: "src-female"}) srcFemale: string = "profile-female.svg";

  /**
   * The type of the id
   */
  @Prop({attribute: "type-id"}) typeId: string = "PC";

  /**
   * The number of the id
   */
  @Prop({attribute: "passport-number"}) passportNumber: string = "AA98765";

  /**
   * The codcountry of the id
   */
  @Prop({attribute: "cod-country"}) codCountry: string = "PRT";

  /**
   * The surname of the id
   */
  @Prop({attribute: "surname-id"}) surnameId: string = "Santos Barbosa";

  /**
   * The givenname of the id
   */
  @Prop({attribute: "given-name"}) givenName: string = "Jorge Cardoso";

  /**
   * The nationality of the id
   */
  @Prop({attribute: "nationality-id"}) nationalityId: string = "Portuguesa";

  /**
   * The height of the id
   */
  @Prop({attribute: "height-id"}) heightId: string = "1.72 m";

  /**
   * The birthdate of the id
   */
  @Prop({attribute: "birth-date"}) birthDate: string = "07.04.1974";

  /**
   * The personalno of the id
   */
  @Prop({attribute: "personal-no"}) personalNo: string = "90876549";

  /**
   * The sex of the id
   */
  @Prop({attribute: "sex-id"}) sexId: "M" | "F" = "M";

  /**
   * The placebirth of the id
   */
  @Prop({attribute: "place-birth"}) placeBirth: string = "Mação*Santarém";

  /**
   * The dateissue of the id
   */
  @Prop({attribute: "date-issue"}) dateIssue: string = "16.02.2020";

  /**
   * The authority of the id
   */
  @Prop({attribute: "authority-id"}) authorityId: string = "SEF - Serv Estr e Fronteiras";

  /**
   * The authority of the id
   */
  @Prop({attribute: "date-expiry"}) dateExpiry: string = "22.06.2028";

  /**
   * The location of the signature image source
   */
  @Prop({attribute: "src-signature"}) srcSignature: string = "signature-id.png";


  render() {
    return(
      <Host>
        <ion-card class="g-passport">

          <ion-card-header>
              <img src={getAssetPath(`../assets/images/${this.srcRepublic}`)} alt="" />
            <ion-text>
              <ion-card-title>República Portuguesa</ion-card-title>
              <ion-card-subtitle>Portuguese Republic</ion-card-subtitle>
            </ion-text>
          </ion-card-header>
          <ion-card-content>

            <ion-grid class="g-passport__content-top">
              <ion-row>
                <ion-col size='4' class="g-passport__profile">
                  <img src={getAssetPath(`../assets/images/${this.srcMale}`)} alt="" />
                </ion-col>
                <ion-col size='8'>
                  <ion-row class="g-passport__content-top-right">
                    <ion-col size='8'>
                      <ion-text>
                        <h2>Passaporte</h2>
                        <h4>Passport</h4>
                      </ion-text>
                    </ion-col>
                    <ion-col size='4'>
                      <img class="g-passport__icon" src={getAssetPath(`../assets/images/${this.srcPassport}`)} alt="" />
                    </ion-col>
                  </ion-row>
                  <ion-row>
                    <ion-col size='2'>
                      <ion-text>
                        <h2>Tipo</h2>
                        <h4>Type</h4>
                        <p>{this.typeId}</p>
                      </ion-text>
                    </ion-col>
                    <ion-col size='5'>
                      <ion-text>
                        <h2>Passaporte nº</h2>
                        <h4>Passport no.</h4>
                        <p>{this.passportNumber}</p>
                      </ion-text>
                    </ion-col>
                    <ion-col size='5'>
                      <ion-text>
                        <h2>Cód. do país</h2>
                        <h4>Code</h4>
                        <p>{this.codCountry}</p>
                      </ion-text>
                    </ion-col>
                  </ion-row>
                </ion-col>
              </ion-row>
            </ion-grid>

            <ion-grid class="g-passport__content-body">
              <ion-row>
                <ion-col size='6'>
                  <ion-label>1.</ion-label>
                  <ion-text>
                    <h2>Apelido</h2>
                    <h4>Surname</h4>
                    <p>{this.surnameId}</p>
                  </ion-text>
                </ion-col>
                <ion-col size='6'>
                  <ion-label>2.</ion-label>
                  <ion-text>
                    <h2>Nomes próprios</h2>
                    <h4>Given names</h4>
                    <p>{this.givenName}</p>
                    </ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col size='6'>
                  <ion-label>3.</ion-label>
                  <ion-text>
                    <h2>Nacionalidade</h2>
                    <h4>Nationality</h4>
                    <p>{this.nationalityId}</p>
                  </ion-text>
                </ion-col>
                <ion-col size='6'>
                  <ion-label>4.</ion-label>
                  <ion-text>
                    <h2>Altura</h2>
                    <h4>Height</h4>
                    <p>{this.heightId}</p>
                  </ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col size='6'>
                  <ion-label>5.</ion-label>
                  <ion-text>
                    <h2>Data de nascimento</h2>
                    <h4>Date of birth</h4>
                    <p>{this.birthDate}</p>
                  </ion-text>
                </ion-col>
                <ion-col size='6'>
                <ion-label>6.</ion-label>
                  <ion-text>
                    <h2>Nº de ident. pessoal</h2>
                    <h4>Personal no.</h4>
                    <p>{this.personalNo}</p>
                  </ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col size='6'>
                <ion-label>7.</ion-label>
                  <ion-text>
                    <h2>Sexo</h2>
                    <h4>Sex</h4>
                    <p>{this.sexId}</p>
                  </ion-text>
                </ion-col>
                <ion-col size='6'>
                <ion-label>8.</ion-label>
                  <ion-text>
                    <h2>Local de nascimento</h2>
                    <h4>Place of birth</h4>
                    <p>{this.placeBirth}</p>
                  </ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col size='6'>
                <ion-label>9.</ion-label>
                  <ion-text>
                    <h2>Data da emissão</h2>
                    <h4>Date of issue</h4>
                    <p>{this.dateIssue}</p>
                  </ion-text>
                </ion-col>
                <ion-col size='6'>
                <ion-label>10.</ion-label>
                  <ion-text>
                    <h2>Autoridade</h2>
                    <h4>Authority</h4>
                    <p>{this.authorityId}</p>
                  </ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col size='6'>
                <ion-label>11.</ion-label>
                  <ion-text>
                    <h2>Válido até</h2>
                    <h4>Date of expiry</h4>
                    <p>{this.dateExpiry}</p>
                  </ion-text>
                </ion-col>
                <ion-col size='6'>
                <ion-label>12.</ion-label>
                  <ion-text>
                    <h2>Assinatura do titular</h2>
                    <h4>Holder's signature</h4>
                    <img src={getAssetPath(`../assets/images/${this.srcSignature}`)} alt="" />
                  </ion-text>
                </ion-col>
              </ion-row>
            </ion-grid>

            <ion-grid class="g-passport__content-bottom">
              <ion-row>
                <ion-col size='12'><p>P&lt;prtsantos&lt;barbosa&lt;ana&lt;&lt;cardoso&lt;&lt;&lt;</p></ion-col>
                <ion-col size='12'><p>AA98765&lt;0PRT9087654904030&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;&lt;81</p></ion-col>
              </ion-row>
            </ion-grid>

          </ion-card-content>
        </ion-card>
      </Host>
    )

  }

}
