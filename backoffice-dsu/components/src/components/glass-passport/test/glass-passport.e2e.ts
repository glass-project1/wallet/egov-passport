import { newE2EPage } from '@stencil/core/testing';

describe('glass-passport', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-passport></glass-passport>');

    const element = await page.find('glass-passport');
    expect(element).toHaveClass('hydrated');
  });
});
