# glass-passport-tk



<!-- Auto Generated Below -->


## Properties

| Property         | Attribute         | Description                                | Type         | Default                          |
| ---------------- | ----------------- | ------------------------------------------ | ------------ | -------------------------------- |
| `authorityId`    | `authority-id`    | The authority of the id                    | `string`     | `"SEF - Serv Estr e Fronteiras"` |
| `birthDate`      | `birth-date`      | The birthdate of the id                    | `string`     | `"07.04.1974"`                   |
| `codCountry`     | `cod-country`     | The codcountry of the id                   | `string`     | `"PRT"`                          |
| `dateExpiry`     | `date-expiry`     | The authority of the id                    | `string`     | `"22.06.2028"`                   |
| `dateIssue`      | `date-issue`      | The dateissue of the id                    | `string`     | `"16.02.2020"`                   |
| `givenName`      | `given-name`      | The givenname of the id                    | `string`     | `"Jorge Cardoso"`                |
| `heightId`       | `height-id`       | The height of the id                       | `string`     | `"1.72 m"`                       |
| `nationalityId`  | `nationality-id`  | The nationality of the id                  | `string`     | `"Portuguesa"`                   |
| `passportNumber` | `passport-number` | The number of the id                       | `string`     | `"AA98765"`                      |
| `personalNo`     | `personal-no`     | The personalno of the id                   | `string`     | `"90876549"`                     |
| `placeBirth`     | `place-birth`     | The placebirth of the id                   | `string`     | `"Mação*Santarém"`               |
| `sexId`          | `sex-id`          | The sex of the id                          | `"F" \| "M"` | `"M"`                            |
| `srcFemale`      | `src-female`      | The location of the profile male icon      | `string`     | `"profile-female.svg"`           |
| `srcMale`        | `src-male`        | The location of the profile male icon      | `string`     | `"profile-male.svg"`             |
| `srcPassport`    | `src-passport`    | The location of the passport source icon   | `string`     | `"passport-icon.svg"`            |
| `srcRepublic`    | `src-republic`    | The location of the republic source icon   | `string`     | `"republic-icon.svg"`            |
| `srcSignature`   | `src-signature`   | The location of the signature image source | `string`     | `"signature-id.png"`             |
| `surnameId`      | `surname-id`      | The surname of the id                      | `string`     | `"Santos Barbosa"`               |
| `typeId`         | `type-id`         | The type of the id                         | `string`     | `"PC"`                           |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
