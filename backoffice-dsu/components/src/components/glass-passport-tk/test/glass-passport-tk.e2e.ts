import { newE2EPage } from '@stencil/core/testing';

describe('glass-passport-tk', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-passport-tk></glass-passport-tk>');

    const element = await page.find('glass-passport-tk');
    expect(element).toHaveClass('hydrated');
  });
});
