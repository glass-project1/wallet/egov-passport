import { newE2EPage } from '@stencil/core/testing';

describe('glass-passport-pt', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-passport-pt></glass-passport-pt>');

    const element = await page.find('glass-passport-pt');
    expect(element).toHaveClass('hydrated');
  });
});
