import { newE2EPage } from '@stencil/core/testing';

describe('glass-residence-permit', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<glass-residence-permit></glass-residence-permit>');

    const element = await page.find('glass-residence-permit');
    expect(element).toHaveClass('hydrated');
  });
});
