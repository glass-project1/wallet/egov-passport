import { newSpecPage } from '@stencil/core/testing';
import { GlassResidencePermit } from '../glass-residence-permit';

describe('glass-residence-permit', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [GlassResidencePermit],
      html: `<glass-residence-permit></glass-residence-permit>`,
    });
    expect(page.root).toEqualHtml(`
      <glass-residence-permit>
        <mock:shadow-root>
          <div class="g-bank">
            <ion-grid>
              <ion-row>
                <ion-col class="g-bank-header">
                  <div class="g-bank-icon">
                    <ion-icon name="checkmark-outline"></ion-icon>
                  </div>
                  <ion-text>
                    <h1>
                      Residence Permit
                    </h1>
                  </ion-text>
                </ion-col>
              </ion-row>
              <ion-row>
                <ion-col class="g-bank-content">
                  <ion-list lines="none">
                    <ion-item>
                      <ion-icon name="reader-outline" slot="start"></ion-icon>
                      <ion-label>
                        <h2>
                          Customer
                        </h2>
                        <p>
                          Lorem ipsum
                        </p>
                      </ion-label>
                    </ion-item>
                    <ion-item>
                      <ion-icon name="reader-outline" slot="start"></ion-icon>
                      <ion-label>
                        <h2>
                          Duration
                        </h2>
                        <p>
                          permanent
                        </p>
                      </ion-label>
                    </ion-item>
                    <ion-item>
                      <ion-icon name="reader-outline" slot="start"></ion-icon>
                      <ion-label>
                        <h2>
                          Type
                        </h2>
                        <p>
                          residence
                        </p>
                      </ion-label>
                    </ion-item>
                  </ion-list>
                </ion-col>
              </ion-row>
            </ion-grid>
          </div>
        </mock:shadow-root>
      </glass-residence-permit>
    `);
  });
});
