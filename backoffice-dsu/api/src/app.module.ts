import { Module } from '@nestjs/common';
import { EGovPassportModule } from './egovpassport/egovpassport.module';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
@Module({
  imports: [EGovPassportModule, AuthModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
