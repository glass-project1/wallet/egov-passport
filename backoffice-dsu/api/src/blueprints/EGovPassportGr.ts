import type {Constructor, DataProperties} from "@glass-project1/dsu-blueprint";
import type {IEGovPassport, ToolkitInjectables} from "@glass-project1/glass-toolkit";

/**
 * {@link RenderableDSUBlueprint} decorated Builtin Class representing an EU Passport
 *
 * @class EGovPassportGr
 * @extends EvidenceBlueprint
 */
export function EGovPassportGr(injectables: ToolkitInjectables): Constructor<IEGovPassport>
export function EGovPassportGr(injectables: ToolkitInjectables, data: DataProperties<IEGovPassport>): IEGovPassport
export function EGovPassportGr(injectables: ToolkitInjectables, data?: DataProperties<IEGovPassport>): Constructor<IEGovPassport> | IEGovPassport {

    @injectables.RenderableDSUBlueprint("glass-passport-gr", {}, undefined, injectables.KeySSIType.SEED, true)
    @injectables.RenderableEvidence("glass-passport-gr", "../components/components", undefined, false)
    class EGovPassportGr extends injectables.EvidenceBlueprint {

        @injectables.uiprop("passport-number")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        passportNumber?: string = undefined;

        @injectables.uiprop("given-name")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        givenName?: string = undefined;

        @injectables.uiprop("surname")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        surname?: string = undefined;

        @injectables.required()
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        issuingDate?: Date = undefined;

        @injectables.required()
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        expirationDate?: Date = undefined;

        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        issuingLocation?: string = undefined;

        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        issuingAuthority?: string = undefined;

        @injectables.uiprop("birth-date")
        @injectables.required()
        @injectables.date("yyyy/MM/dd")
        @injectables.dsuEvidenceField(true, false, true)
        birthDate?: Date = undefined;

        @injectables.uiprop("birth-location")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        birthLocation?: string = undefined;

        @injectables.uiprop("nationality")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        nationality?: string = undefined;

        @injectables.uiprop("sex")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        gender?: string = undefined;

        @injectables.uiprop("height")
        @injectables.required()
        @injectables.dsuEvidenceField(true, false, true)
        height?: string = undefined;

        constructor(EGovPassportGr?: EGovPassportGr | {}) {
            super(EGovPassportGr);
            injectables.constructFromBlueprint<EGovPassportGr>(this, EGovPassportGr);
        }
    }

    if (data)
        return new EGovPassportGr(data);
    return EGovPassportGr;
}