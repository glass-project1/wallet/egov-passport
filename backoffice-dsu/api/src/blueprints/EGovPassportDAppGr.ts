import type {Constructor} from "@glass-project1/dsu-blueprint";
import type {IEGovPassport, IEGovPassportDApp, IGlassDID} from "@glass-project1/glass-toolkit";
import type {DSUDatabase, DSUEnclave, DSUSecurityContext, EnvironmentDefinition} from "@glass-project1/opendsu-types";
import type {EGovPassportInjectables} from "./types";

/**
 * {@link DSUBlueprint} decorated Builtin Class representing the {@link EGovPassportDAppGr}
 *
 * Note that it contains an {@link EGovPassport} under 'passport'
 *
 * @class EGovPassportDAppGr
 * @extends DBModel
 *
 */
export function EGovPassportDAppGr(injectables: EGovPassportInjectables): Constructor<IEGovPassportDApp>
export function EGovPassportDAppGr(injectables: EGovPassportInjectables, data: Record<string, any>): IEGovPassportDApp
export function EGovPassportDAppGr(injectables: EGovPassportInjectables, data?: Record<string, any>): Constructor<IEGovPassportDApp> | IEGovPassportDApp {

    const eGovPassport = injectables.blueprints.EGovPassportGr(injectables);

    @injectables.DSUBlueprint(undefined, injectables.KeySSIType.SEED)
    class EGovPassportDAppGr extends injectables.DBModel {

        @injectables.dsuMixinBlueprint(injectables.blueprints.EGovPassportGr, injectables, true, true)
        passport?: IEGovPassport = undefined;

        @injectables.signedDID(undefined)
        did?: IGlassDID = undefined;

        @injectables.enclave()
        enclave?: DSUEnclave = undefined;

        @injectables.environment()
        environment?: EnvironmentDefinition = undefined;

        @injectables.walletDB()
        db?: DSUDatabase = undefined;

        @injectables.securityContext()
        sc?: DSUSecurityContext = undefined;

        constructor(eGovIDdApp?: EGovPassportDAppGr | {}) {
            super();
            injectables.constructFromBlueprint<EGovPassportDAppGr>(this, eGovIDdApp);
            this.passport = new eGovPassport(this.passport);
            this.did = new injectables.GlassDID(this.did);
        }
    }

    if (data)
        return new EGovPassportDAppGr(data);
    return EGovPassportDAppGr;
}