import {getBlueprintInjectables, setBlueprintInjectables} from "@glass-project1/dsu-blueprint";
import {toolkitInjectables} from "@glass-project1/glass-toolkit";
import {EGovPassportDAppGr} from "./EGovPassportDAppGr";
import {EGovPassportDAppPt} from "./EGovPassportDAppPt";
import {EGovPassportDAppTk} from "./EGovPassportDAppTk";
import {EGovPassportGr} from "./EGovPassportGr";
import {EGovPassportPt} from "./EGovPassportPt";
import {EGovPassportTk} from "./EGovPassportTk";

import type {EGovPassportInjectables} from "./types";

export const eGovPassportInjectables: EGovPassportInjectables = {
    ...toolkitInjectables,
    blueprints: {
        ...toolkitInjectables.blueprints,

        EGovPassportGr,
        EGovPassportPt,
        EGovPassportTk,

        EGovPassportDAppGr,
        EGovPassportDAppPt,
        EGovPassportDAppTk,
    }
}

export function getEGovPassportInjectables(): EGovPassportInjectables {
    setBlueprintInjectables(eGovPassportInjectables, true);
    return getBlueprintInjectables() as EGovPassportInjectables;
}