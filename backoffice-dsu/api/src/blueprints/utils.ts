import {Constructor} from "@glass-project1/dsu-blueprint";
import {eGovPassportInjectables} from "./constants";
import type {EGovPassportInjectables} from "./types";

type EGovIdBlueprintFunction<T> = (injectables: EGovPassportInjectables) => Constructor<T>;

export function getBlueprint<T>(blueprintName: string, countryCode: string): EGovIdBlueprintFunction<T> | undefined {
    countryCode = countryCode.toLowerCase();
    countryCode = countryCode.charAt(0).toUpperCase() + countryCode.slice(1);
    return eGovPassportInjectables.blueprints[blueprintName + countryCode];
}