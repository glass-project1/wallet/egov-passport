
import { HttpException, HttpStatus } from '@nestjs/common';
import { PaginatedDto } from 'src/paginated.dto';
import { createQueryBuilder, EntityRepository, getManager, Repository } from 'typeorm';
import { EGovPassportEntity } from './egovpassport.entity';
import { EGovPassportQuery } from './egovpassportquery.validator';

@EntityRepository(EGovPassportEntity)
export class EGovPassportRepository extends Repository<EGovPassportEntity>  {
    constructor(
    ) {
        super();
    }

    /**
     * Performs a SQL query applying the filters according to the @param
     * @param eGovPassportSearchQuery
     */
     async search(eGovPassportSearchQuery: EGovPassportQuery): Promise<PaginatedDto<EGovPassportQuery,EGovPassportEntity>> {
        console.log('egovpassport.repository.search query=', eGovPassportSearchQuery)

        const escapeQuote = (str : string): string => {
            if (typeof str === 'string')
                return str.replace(/'/g, "''");
            else
                return str;
        };

        const transformValueToCommaList = (arr: string[] | string): string => {
            arr = Array.isArray(arr) ? arr : [arr]
            return arr.map(value => `'${escapeQuote(value)}'`).join(',');
        }

        const transformValueToEqualOrList = (fieldName: string, arr: string[] | string): string => {
            arr = Array.isArray(arr) ? arr : [arr]
            return "("+arr.map(value => `${fieldName} = '${escapeQuote(value)}'`).join(' OR ')+")";
        }

        const transformValueToLikeList = (fieldName: string, value: string[] | string): string => {
            const values = Array.isArray(value) ? value : [value];
            let str = '';
            let sep = '';
            values.forEach((value: string, index: number) => {
                str += sep;
                str += `${fieldName} ILIKE '%${escapeQuote(value)}%'`;
                sep = ' OR ';
            });
            return "( "+str+" )";
        }

        const getJsonWhereStatement = (fieldName: string, jsonProperty: string, values: string[] | string): string => {
            values = Array.isArray(values) ? values : [values]
            let str = ''
            values.forEach((value: string, index: number) => {
                if (index == 0) {
                    str += `${fieldName} ::jsonb @> \'{"${jsonProperty}":"${escapeQuote(value)}"}\'`
                } else {
                    str += `OR ${fieldName} ::jsonb @> \'{"${jsonProperty}":"${escapeQuote(value)}"}\'`
                }
            })
            return "( "+str+" )";
        }

        const getJsonWhereFieldLikeStatement = (fieldName: string, jsonProperty: string, values: string[] | string): string => {
            values = Array.isArray(values) ? values : [values];
            let str = '';
            let sep = '';
            values.forEach((value: string, index: number) => {
                str += sep;
                str += `${fieldName}::jsonb->>'${jsonProperty}' ILIKE '%${escapeQuote(value)}%'`;
                sep = ' OR ';
            });
            return "( "+str+" )";
        }

        /** NOTE: The name of "whereFunctions" need to be the same name of filter/properties of EventSearchQuery */
        const whereFunctions = {
            id(id: string[] | string): string {
                return `egovpassport.id IN (${transformValueToCommaList(id)})`;
            },
            passportNumber(str: string[]  | string): string {
                return `egovpassport.passportNumber IN (${transformValueToCommaList(str)})`;
            },
            givenName(str: string[]  | string): string {
                return transformValueToLikeList('egovpassport.givenname', str);
            },
            surname(str: string[]  | string): string {
                return transformValueToLikeList('egovpassport.surname', str);
            },
            issuingDateStart(date: string): string {
                return `egovpassport.issuingDate>=('${escapeQuote(date)}'::TIMESTAMPTZ)`; // check for injection issues ?
            },
            issuingDateEnd(date: string): string {
                return `egovpassport.issuingDate<('${escapeQuote(date)}'::TIMESTAMPTZ)`; // check for injection issues?
            },
            birthDateStart(date: string): string {
                return `egovpassport.birthDate>=('${escapeQuote(date)}'::TIMESTAMPTZ)`; // check for injection issues ?
            },
            birthDateEnd(date: string): string {
                return `egovpassport.birthDate<('${escapeQuote(date)}'::TIMESTAMPTZ)`; // check for injection issues?
            },
        }
        const sortProperties = {
            // prop names must match EGovIdQuerySortProperty
            "id":                 "egovpassport.id",
            "passportNumber":     "egovpassport.passportNumber",
            "givenName":          "egovpassport.givenName",
            "surname":            "egovpassport.surname",
            "issuingDate":        "egovpassport.issuingDate",
            "birthDate":          "egovpassport.birthDate",
        };

        let queryBuilder = await createQueryBuilder(EGovPassportEntity, 'egovpassport');
        //let whereSql : string = '';
        //let whereSqlSep : string = ' AND ';
        for (let [filterName, filterValue] of Object.entries(eGovPassportSearchQuery)) {
            const whereFilter = whereFunctions[filterName]
            if (!!whereFilter) {
                const whereSqlClause = whereFilter(filterValue);
                queryBuilder.andWhere(whereSqlClause);
                //whereSql += `${whereSqlSep}${whereSqlClause}`;
            }
        }
        const orderByProps = Array.isArray(eGovPassportSearchQuery.sortProperty) ? eGovPassportSearchQuery.sortProperty : [eGovPassportSearchQuery.sortProperty];
        const orderByDirs  = Array.isArray(eGovPassportSearchQuery.sortDirection) ? eGovPassportSearchQuery.sortDirection : [eGovPassportSearchQuery.sortDirection];
        if (orderByProps.length != orderByDirs.length) {
            throw new HttpException('sortProperty and sortDirection must have the sane number of values', HttpStatus.INTERNAL_SERVER_ERROR);
        }
        //let sortSql: string = '';
        //let sortSqlSep: string = '';
        let i: number = 0;
        for(i = 0; i<orderByProps.length; i++) {
            const orderByProp = orderByProps[i];
            let sortProp = sortProperties[orderByProp];
            if (!sortProp) {
                throw new HttpException('sortProperty value unsupported. See possible values.', HttpStatus.INTERNAL_SERVER_ERROR);
            }
            const orderByDir = orderByDirs[i];
            queryBuilder.addOrderBy(sortProp, orderByDir);
            //sortSql += `${sortSqlSep}${sortProp} ${orderByDir}`;
            //sortSqlSep = ',';
        }

        console.log(queryBuilder.getSql());
        const count = await queryBuilder.getCount();
        queryBuilder.take(eGovPassportSearchQuery.limit)
        queryBuilder.skip(eGovPassportSearchQuery.page * eGovPassportSearchQuery.limit)
        const egovpassportCollection = await queryBuilder.getMany();

        return {count: count, query: eGovPassportSearchQuery, results: egovpassportCollection };
    }
}
