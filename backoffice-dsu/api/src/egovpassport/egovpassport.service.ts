import {inject} from "@glass-project1/db-decorators";
import {GlassWalletManager, IEGovPassportDApp, MarketKeys} from '@glass-project1/glass-toolkit';
import {generateWalletEnvironment, getEnvironmentFromProcess} from "@glass-project1/glass-toolkit/lib/providers";
import {debug, info, LoggedError} from '@glass-project1/logging';
import {DSUDid} from "@glass-project1/opendsu-types";
import {HttpService} from '@nestjs/axios';
import {Injectable, NotFoundException} from '@nestjs/common';
import {Connection, EntityManager} from 'typeorm';
import {getEGovPassportInjectables} from "../blueprints/constants";
import {getBlueprint} from "../blueprints/utils";
import {ServiceWalletBooter} from "../ServiceWalletBooter";
import {AppResourceRepository} from './appresource.repository';
import {EGovPassportEntity} from './egovpassport.entity';
import {EGovPassportRepository} from './egovpassport.repository';

/*
   Imports from outside the api folder will force tsconfig to re-create the dist folder structure.
   These where used for the model classes, but once the model moved into glass-toolkit,
   it is no longer needed!
   privatesky is imported using require.
*/
// import { Dummy } from "../../../src/model"; // this works, but forces dist/api/src
// import { EGovId } from "./model";
/* the above line does not work, even if adding to tsconfig.json

    "compilerOptions": {
      "rootDirs": ["./src", "../src"] // or "../../src"
    }
*/
//import {safeParseKeySSI} from "@glass-project1/dsu-blueprint/lib";
//import opendsu = require('../../../privatesky/psknode/bundles/openDSU'); // if here, will break https://gitlab.com/glass-project1/wallet/egov-id/-/issues/1#note_27486


@Injectable()
export class EGovPassportService {

    private eGovPassportRepository: EGovPassportRepository;
    arcRepository: AppResourceRepository;

    @inject("GlassWalletManager")
    walletManager: GlassWalletManager;

    @inject("ServiceWalletBooter")
    walletBooter!: ServiceWalletBooter;
    
    constructor(
        private connection: Connection,
        private readonly httpService: HttpService,
    ) {
        this.arcRepository = this.connection.getCustomRepository(AppResourceRepository);
        this.eGovPassportRepository = this.connection.getCustomRepository(EGovPassportRepository);
    }

 

    /**
     * Create (INSERT) a new EGovPassport from DTO JSON data in a single transaction.
     * @param eGovPassportDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async create(eGovPassportDto: any) {
        const self: EGovPassportService = this;
        await this.connection.transaction(async tem => {
            await self.createT(tem, eGovPassportDto);
        });
    }

    /**
     * Create (INSERT) a new ClinicalTrial from DTO JSON data, given a transactional entity manager.
     * @param tem Transactional EntityManager
     * @param egovpassportDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async createT(tem: EntityManager, egovpassportDto: any) {
        await tem.save(EGovPassportEntity, egovpassportDto);
    }

    /**
     * Update (SQL UPDATE) a EGovId from DTO JSON data in a single transaction.
     * @param eGovPassportDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async update(eGovPassportDto: any) {
        const self = this;
        await this.connection.transaction(async tem => {
            await self.updateT(tem, eGovPassportDto);
        });
    }

    /**
     * Update (SQL UPDATE) a EGovPassport from DTO JSON data in a single transaction.
     * @param tem Transactional EntityManager
     * @param eGovPassportDto data to be inserted, from JSON. Will be mutated by adding PKs and internal FKs.
     */
    async updateT(tem: EntityManager, eGovPassportDto: any) {
        await tem.save(EGovPassportEntity, eGovPassportDto); // autocommit is good enough ?
    }

    /**
     * Create a new DSU wallet for a given ID number.
     * If this ID already has a wallet, the same wallet is returned.
     * No more than one wallet can be created for the same ID.
     *
     * The algorithm is as follows:
     * step 1 - BEGIN TRANSACTION
     * step 2 - SELECT FOR UPDATE to lock the record being modified.
     * step 3 - If alredy has a wallet, return it and stop here.
     * step 4 - Create the DSU using an external DSU service with a callback
     * step 5 - UPDATE the record previously locked
     * step 6 - COMMIT
     *
     * Error handling must rollback and release the transaction.
     *
     * @param {string} eGovPassportId ID of the record to create the wallet.
     */
    async createWallet(eGovPassportId: string): Promise<EGovPassportEntity> {
        const self = this;
        const debugId = "egovpassport createWallet "+eGovPassportId;
        let eGovPassportEntity: EGovPassportEntity = undefined;

        let resultResolve;
        let resultReject;
        const result = new Promise<EGovPassportEntity>((resolve, reject) => {
            resultResolve = resolve;
            resultReject = reject;
        });

        const queryRunner = this.connection.createQueryRunner();
        await queryRunner.startTransaction();
        result.catch(async (err) => {
            // from now on, if we reject the result, we need to rollback and close the DB query.
            // self.releaseOpenDSU(openDsuLockedFlag, debugId);
            await queryRunner.rollbackTransaction();
            await queryRunner.release();
            // throw new Error(err); // do not (re) throw, or nestjs will die. Rejection is enough.
        });
        try {
            eGovPassportEntity = await queryRunner.manager.findOne(EGovPassportEntity, eGovPassportId, { lock: { mode: "pessimistic_write" } });
            if (!eGovPassportEntity) {
                //await queryRunner.rollbackTransaction();
                //await queryRunner.release();
                throw new NotFoundException("No EGovPassport.id=" + eGovPassportId);
            }

            if (eGovPassportEntity.walletKeySSI) {
                // keySSI already filled, means that wallet is already existing.
                // self.releaseOpenDSU(openDsuLockedFlag, debugId);
                await queryRunner.rollbackTransaction();
                await queryRunner.release();
                resultResolve(eGovPassportEntity);
                return result; // createWallet must return the promise
            }


            // egov-id #22 - one OpenDSU writing per domain at a time. Acquire lock just to get root wallet.
            // openDsuLockedFlag = await self.acquireOpenDSU(debugId);

            /*
            let keyssispace = getKeySSIApi();
            const aSeedSSI = keyssispace.createTemplateSeedSSI("egovid", `STWHS000001-WHS000001-${(new Date()).toISOString()}`, 'v0');
            console.log(aSeedSSI.getIdentifier(true));
            */

            const agencyDID = await self.getAgencyDID();

            const env: {domain: string, didDomain: string, vaultDomain: string} = getEnvironmentFromProcess();

            debug.call(self, "Environment from process is {0}", JSON.stringify(env, undefined, 2))

            const EGovPassportDApp = getBlueprint<IEGovPassportDApp>("EGovPassportDApp", self.walletBooter.countryCode);
            if (!EGovPassportDApp)
                return resultReject(new LoggedError(`Invalid EGovIdDApp for ${self.walletBooter.countryCode} country code`, self));

            const EGovPassportDAppConstr = EGovPassportDApp(getEGovPassportInjectables());
            const eGovPassportDAppDSU = new EGovPassportDAppConstr({
                passport:{
                    ... eGovPassportEntity,
                    __metadata: {
                        designation: {
                            en: "Passport"

                        },
                        identifier: `egov.${self.walletBooter.countryCode}.ibs.passport.${eGovPassportEntity.id}`,
                    }
                },
                did: {
                    data : {
                        name: [eGovPassportEntity.givenName, eGovPassportEntity.surname].join(' '),
                        identifier: `individual.egov.${self.walletBooter.countryCode}.ibs.passport.${eGovPassportEntity.id}`,
                        publishes: [
                            `individual.egov.${self.walletBooter.countryCode}.ibs.passport.${eGovPassportEntity.id}.**.*`
                        ]
                    }
                },
                environment: generateWalletEnvironment("egovpassport-dapp", env)
            });

            const keyCache : {domain: string, [indexer: string]: any} = {"domain": env.domain};
            keyCache[MarketKeys.DOMAIN] = env.domain; //sets the market domain flag

            //const repo = new OpenDSURepository(EGovPassportDApp, env.domain, agencyDID, undefined, keyCache);
            // eGovPassportDAppDSU.did = undefined; egov#4 uncomment to force error on callback
            //repo.create(eGovPassportDAppDSU, async (err, updatedModel, dsu, keySSI) => {
            self.walletManager.issueEvidence(keyCache, eGovPassportDAppDSU, undefined, EGovPassportDApp, async (err, updatedModel, dsu, keySSI) => {
                debug(`walletManager.issueEvidence eGovPassportDAppDSU returned ${err}, ${updatedModel}, ${keySSI ? keySSI.getIdentifier(true) : keySSI, eGovPassportDAppDSU}`);
                if (err) {
                    // throw err; egov#4 throw is not always caught by catch
                    //await queryRunner.rollbackTransaction();
                    //await queryRunner.release();
                    return resultReject(new LoggedError(err, self));
                }
                eGovPassportEntity.walletKeySSI = keySSI.getIdentifier(true);
                //await this.enrichKeySSI(eGovPassportEntity);
                await queryRunner.manager.save(EGovPassportEntity, eGovPassportEntity);
                // self.releaseOpenDSU(openDsuLockedFlag, debugId);
                await queryRunner.commitTransaction();
                await queryRunner.release();
                resultResolve(eGovPassportEntity);
                return; // callback has to return nothing
            });
        } catch (err) {
            //await queryRunner.rollbackTransaction();
            //await queryRunner.release();
            resultReject(err);
        }

        // Don't add the finally. It will be called before the repo.create(...) calls the calback,
        // and we do not want to release the transaction
        //finally {
        //    await queryRunner.release();
        //}

        return result;
    }


    /**
     * Experimental one time security context + DID setup.
     * TODO - needs external protection against concurrent races.
     */
    async getAgencyDID(): Promise<DSUDid> {

        let resultResolve;
        let resultReject;
        const result = new Promise<DSUDid>((resolve, reject) => {
            resultResolve = resolve;
            resultReject = reject;
        });

        //console.log("Wallet returned", wallet);
        resultResolve(this.walletManager.did);

        /*
        getWallet((err, wallet) => {
            if (err)
                return resultReject(err);

            if (!wallet || !wallet.did)
                return resultReject(new Error('Missing data'));

            resultResolve(wallet.did);
        })
        */
        //
        // const id = "COUNTRY_GOV_EID_AGENCY";
        // try {
        //     const sc: DSUSecurityContext = getSCApi().refreshSecurityContext();
        //     sc.on(OpenDSUInitializationEvent, () => {
        //         getDSUDidFactoryRegistry().build(DSUDIDMethods.NAME, "default", id, (err: Err, didDocument: DSUDid) => {
        //             if (err)
        //                 throw err;
        //            resultResolve(didDocument);
        //         });
        //     });
        // } catch (err) {
        //     resultReject(err);
        // }

        return result;
    }


    /**
     * Experimental wallet creation.
     * TODO - needs external protection against concurrent races between read/create.
     */
    // async getWallet(): Promise<EGovServiceWallet> {}

    decodeWalletKeySSI(eGovPassportEntity : EGovPassportEntity): string | undefined {
        if (!eGovPassportEntity || !eGovPassportEntity.walletKeySSI)
            return undefined;

        // if it is already JSON, do not enrich it
        let alreadyJson = false;
        let parsedJson : any = {};

        try {
            parsedJson = JSON.parse(eGovPassportEntity.walletKeySSI);
            alreadyJson = true;
        } catch (e) {
            ;
        }
        if (alreadyJson)
            return parsedJson.payload.keySSI; // TODO not sure this is right
        else
            return eGovPassportEntity.walletKeySSI;
    }

    async parentCreateNewServiceWallet(schema: {}) : Promise<string> {
        const self = this;
        const parentBorestUrl = await self.parentGetRestUrl();
        const postUrl = parentBorestUrl+"/egov/egov/setup/createNewServiceWallet";
        const env = self.walletBooter.environment;
        const data = { didSchema: schema, domain: env.domain || 'undefined-domain' };
        try {
            info("POST "+postUrl+" "+JSON.stringify(data));
            const parentAxiosReponse = await self.httpService.axiosRef.post(postUrl, data);
            const serviceWalletSeedSSI : string = parentAxiosReponse.data;
            //console.log(serviceWalletSeedSSI);
            info("returned "+serviceWalletSeedSSI+" from POST "+postUrl);
            return serviceWalletSeedSSI;
        } catch (err) {
            if (err && err.response && err.response.data && err.response.data.message) {
                throw new Error(`POST ${postUrl} : ${err.response.data.statusCode} ${err.response.data.message}`);
            } else {
                throw new Error(`POST ${postUrl} : Error ${JSON.stringify(err)}`);
            };
        }
    }

    async parentGetRestUrl() : Promise<string> {
        const self = this;
        const parentBorestUrl = await self.arcRepository.findConfigString("egovpassport.parent.borestUrl");
        info("parent/borest URL is {0}", parentBorestUrl);
        return parentBorestUrl;
    }
}

