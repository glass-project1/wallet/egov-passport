import { Connection } from "typeorm";
import { Controller, Get, Put, Param, Body, Post, UseGuards, Query, NotFoundException } from '@nestjs/common';
import { AuthGuard } from "@nestjs/passport";
import { ApiBearerAuth, ApiOperation, ApiTags, ApiParam, getSchemaPath, ApiExtraModels, ApiOkResponse } from "@nestjs/swagger";
import { EGovPassportEntity } from './egovpassport.entity';
import { EGovPassportService } from './egovpassport.service';
import { EGovPassportQuery, EGovPassportQueryValidator } from "./egovpassportquery.validator";
import { EGovPassportRepository } from "./egovpassport.repository";
import { PaginatedDto } from "../paginated.dto";




@ApiExtraModels(PaginatedDto)
@ApiTags('EGovPassport')
//@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('/egovpassport/egovpassport')
export class EGovPassportController {

    private eGovPassportRepository: EGovPassportRepository;

    constructor(
        private connection: Connection,
        private eGovPassportService: EGovPassportService
    ) {
        this.eGovPassportRepository = this.connection.getCustomRepository(EGovPassportRepository);
    }

    @Get()
    @ApiOperation({summary: "Search for EGovPassport based on a query, with paginated results."})
    @ApiOkResponse({
        schema: {
            allOf: [
                { $ref: getSchemaPath(PaginatedDto) },
                {
                    properties: {
                        results: {
                            type: 'array',
                            items: { $ref: getSchemaPath(EGovPassportEntity) },
                        },
                    },
                },
            ],
        },
    })
    async search(@Query(EGovPassportQueryValidator) eGovPassportQuery: EGovPassportQuery): Promise<PaginatedDto<EGovPassportQuery, EGovPassportEntity>> {
        console.log("egovpassport.controller.search... query=", eGovPassportQuery);
        const page = await this.eGovPassportRepository.search(eGovPassportQuery);
        console.log("egovpassport.controller.search results =", page);
        return page;
    }

    @Get(":id")
    @ApiOperation({ summary: 'Get one EGovPassport record' })
    @ApiParam({ name: 'id', type: String, description: "Exact match of primary key column EGovPassport.id" })
    @ApiOkResponse({
        type: EGovPassportEntity
    })
    async findOne(@Param() params): Promise<EGovPassportEntity> {
        console.log("egovpassport.controller.findOne... id=", params.id);
        let eGovPassport = await EGovPassportEntity.findOne(params.id);
        if (!eGovPassport) throw new NotFoundException(`Not found EGovPassport.id="${params.id}"`);
        console.log("egovpassport.controller.findOne egovpassport =", eGovPassport);
        return eGovPassport;
    }

    /* don't allow  update PUT */

    @Post(":id")
    @ApiOperation({ summary: 'Create one wallet for a given ID' })
    @ApiParam({ name: 'id', type: String })
    @ApiOkResponse({
        status: 201,
        type: EGovPassportEntity
    })
    async createWallet(@Param() params, @Body() body: any): Promise<EGovPassportEntity> {
        console.log(`egovpassport.controller.post/${params.id}...body`, body);

        const eGovPassport = await this.eGovPassportService.createWallet(params.id);
    
        console.log("egovpassport.controller.post/${params.id} DB connection closed, egovpassport =", eGovPassport);
        return eGovPassport;
    }


    @Get("/setup/onetime")
    @ApiOperation({ summary: 'Setup a wallet with a DID for this government agency. No need to invoke explicitely, as it will be invoked internally on the 1st use.' })
    @ApiOkResponse({ description: "The returned string is the agency's DID. (Even if invoked more than once, the agency DID is always the same)."})
    async setup() : Promise<string> {
        console.log(`egovpassport.controller.setup`);

        const agencyDid = await this.eGovPassportService.getAgencyDID();
    
        console.log("egovpassport.controller.setup =", agencyDid);
        return agencyDid.getIdentifier();
    }

}

