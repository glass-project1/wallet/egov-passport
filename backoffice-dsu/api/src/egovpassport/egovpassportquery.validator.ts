import { ArgumentMetadata, BadRequestException, Injectable, PipeTransform } from "@nestjs/common"
import { IsDateString, IsEnum, IsInt, IsNumber, IsOptional, IsString, Min, validate } from "class-validator"
import { plainToClass, Transform } from "class-transformer"
import { ApiProperty } from "@nestjs/swagger"

@Injectable()
export class EGovPassportQueryValidator implements PipeTransform<EGovPassportQuery> {
    async transform(value: object, metadata: ArgumentMetadata): Promise<EGovPassportQuery> {
        console.log('egovpassportquery.validator.transform raw=', value)
        const search = plainToClass(metadata.metatype, value)
        const errors = await validate(search, { skipMissingProperties: false, whitelist: true, transform: true })
        if (errors.length > 0) {
            const message = Object.values(errors[0].constraints).join(". ").trim()
            throw new BadRequestException(message)
        }
        console.log('egovpassportquery.validator.transform return=', search)
        return search
    }
}

export enum EGovPassportQuerySortProperty {
    ID = "id",
    PASSPORT_NUMBER = "passportNumber",
    GIVEN_NAME = "givenName",
    SURNAME = "surname",
    ISSUING_DATE = "issuingDate",
    BIRTH_DATE = "birthDate"
};
export enum EGovPassportQuerySortDirection {
    ASC = "ASC",
    DESC = "DESC"
};
export class EGovPassportQuery {

    @ApiProperty({ required: false, description: "Filter by exact match to EGovPassport.id" })
    @IsOptional()
    @IsString({ each: true })
    id: string;

    @ApiProperty({ required: false, description: "Filter by substring match to EGovPassport.passportNumber" })
    @IsOptional()
    @IsString({ each: true })
    passportNumber: string;

    @ApiProperty({ required: false, description: "Filter by substring match to EGovPassport.givenName" })
    @IsOptional()
    @IsString({ each: true })
    givenName: string;

    @ApiProperty({ required: false, description: "Filter by substring match to EGovPassport.surname" })
    @IsOptional()
    @IsString({ each: true })
    surname: string;

    @ApiProperty({
        type: Date,
        isArray: false,
        example: '1960-12-29',
        required: false, description: "Filter by EGovPassport.issuingDate >= (inclusive lower bound)" })
    @IsOptional()
    @IsDateString()
    issuingDateStart: Date;

    @ApiProperty({
        type: Date,
        isArray: false,
        example: '2029-12-31',
        required: false, description: "Filter by EGovPassport.issuingDate < (exclusive upper bound)" })
    @IsOptional()
    @IsDateString()
    issuingDateEnd: Date;

    @ApiProperty({
        type: Date,
        isArray: false,
        example: '1932-12-29',
        required: false, description: "Filter by EGovPassport.birthDate >= (inclusive lower bound)" })
    @IsOptional()
    @IsDateString()
    birthDateStart: Date;

    @ApiProperty({
        type: Date,
        isArray: false,
        example: '2021-12-31',
        required: false, description: "Filter by EGovPassport.birthDate < (exclusive upper bound)" })
    @IsOptional()
    @IsDateString()
    birthDateEnd: Date;

    @ApiProperty({ required: false, description: "Number of items per page. Defaults to 10." })
    @IsOptional()
    @IsInt()
    @Min(1)
    @Transform(({ value }) => parseInt(value))
    limit: number = 10;

    @ApiProperty({ required: false, description: "Page number. Starts at zero. Defaults to zero." })
    @IsOptional()
    @IsInt()
    @Min(0)
    @Transform(({ value }) => parseInt(value))
    page: number = 0;

    @ApiProperty({ required: false, description: "Sort property name. Defaults to 'givenName'. Possible values are 'id', 'passportNumber', 'givenName', 'surname'." })
    @IsOptional()
    @IsEnum(EGovPassportQuerySortProperty, { each: true })
    sortProperty: EGovPassportQuerySortProperty = EGovPassportQuerySortProperty.ID;

    @ApiProperty({ required: false, description: "Sort property order. Use ASC or DESC. Defaults to ASC." })
    @IsOptional()
    @Transform(({ value }) => {
        return value.toUpperCase()
    })
    @IsEnum(EGovPassportQuerySortDirection, { each: true })
    sortDirection: EGovPassportQuerySortDirection = EGovPassportQuerySortDirection.ASC;

};