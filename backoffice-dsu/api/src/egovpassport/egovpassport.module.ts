import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppResourceController } from './appresource.controller';
import { EGovPassportController } from './egovpassport.controller';
import { EGovPassportService } from './egovpassport.service';
import { AppUserService } from './appuser.service';
import {SefController} from "./sef.controller";
import {SefService} from "./sef.service";
import { SefTempController } from './seftemp.controller';
import { SefTempService } from './seftemp.service';

@Module({
  imports: [HttpModule,
    TypeOrmModule.forRoot({
    "name": "default",
    "type": "postgres",
    "host": ( process.env.EGOVPASSPORTDB_HOST || "localhost" ),
    "port": ( process.env.EGOVPASSPORTDB_PORT ? parseInt(process.env.EGOVPASSPORTDB_PORT) : 5432 ),
    "username": "egovpassport",
    "password": "egovpassport",
    "database": "egovpassport",
    "entities": [
      "dist/egovpassport/*.entity.js"
    ],
    "synchronize": false,
    "logging": true
  })],
  controllers: [
    AppResourceController,
    EGovPassportController,
    SefController,
    SefTempController
  ],
  providers: [
    EGovPassportService,
    AppUserService,
    SefService,
    SefTempService
  ],
  exports: [
    AppUserService,
    EGovPassportService
  ],
})
export class EGovPassportModule { }
