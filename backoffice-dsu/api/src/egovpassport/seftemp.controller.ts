import { Controller, Param, Body, Post} from '@nestjs/common';
import { ApiBearerAuth, ApiOperation, ApiTags, ApiParam, ApiResponse } from "@nestjs/swagger";
import { SefTempService } from './seftemp.service';




@ApiTags('SEF')
//@UseGuards(AuthGuard('jwt'))
@ApiBearerAuth()
@Controller('/egovpassport/seftemp')
export class SefTempController {

    constructor(
        private sefService: SefTempService
    ) {
    }

    @Post(":did")
    @ApiOperation({summary: 'Create one GlassEvidenceRequest for a given receiverDid'})
    @ApiParam({name: 'did', type: String, description: "Receiver DID"})
    @ApiResponse({ status: 201, description: 'The GlassEvidenceRequest object created.'})
    async create(@Param() params, @Body() body: any): Promise<any> {
        console.log(`sef.controller.post/${params.did}...body`, body);

        const result = await this.sefService.create(params.did);

        console.log(`sef.controller.post/${params.did} = ${result}`);
        return result;
    }
}
