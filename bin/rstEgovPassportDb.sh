#!/bin/bash -xe
# Tool to reset database in egov-id
MY_PWD=${PWD##*/}          # to assign to a variable
MY_PWD=${MY_PWD:-/}        # to correct for the case where PWD=/
if [ "$MY_PWD" != egov-passport ]
then
    echo 1>&2 "Current working dir must be egov-passport"
    exit 1
fi
cd backoffice-sql
PGPASSWORD=egovpassport psql -h localhost egovpassport egovpassport <<EOF
DROP OWNED BY egovpassport;
\i lib/sql/install/egovpassport.sql
EOF
