#!/bin/bash -x
# this is used by ../docker/docker-compose.yml
# it runs (inside the docker only) the egovpassport.sql script with the user egovpassport
set -e
# remove ON_ERROR_STOP=1 to ignore errors
#psql -v ON_ERROR_STOP=1 --username "egovpassport" --dbname "egovpassport" -f /docker-entrypoint-initdb.d/60egovpassport.norun
# removed because there is an error on a COMMENT, ERROR:  must be owner of extension uuid-ossp
psql -v --username "egovpassport" --dbname "egovpassport" -f /docker-entrypoint-initdb.d/60egovpassport.norun
if [ -n "$GLASS_COUNTRY" ]
then
    if [[ ! "$GLASS_COUNTRY" =~ [A-ZA-Z] ]] ; then echo 2>&1 "env GLASS_COUNTRY must be an ISO 2 letter code upper-case" ; exit 1 ; fi
    GLASS_COUNTRY_LC=$(echo "$GLASS_COUNTRY" | tr '[:upper:]' '[:lower:]')
    psql --echo-all -v --username "egovpassport" --dbname "egovpassport" <<EOF
DELETE FROM egovpassport WHERE country != '${GLASS_COUNTRY}';
UPDATE appresource
    SET value='http://egov-${GLASS_COUNTRY_LC}/borest'
    WHERE key='egovpassport.parent.borestUrl';
EOF
else
    echo "WARNING: environment variable GLASS_COUNTRY not set! Preserving default appresource URLs" 2>&1
fi
