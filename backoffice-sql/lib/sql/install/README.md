# sql/install

The scripts under this install folder are supposed to run only once, when the database is setup for the 1st time.

## Files

```sh
setup.sql - create user and databse egovpassport (to run as PostgreSQL superuser - typically postgres)
egovpassport.sql - create and populate egovpassport database (to run as egovpassport user)
egovpassport_run.sh - to be used from the top level Dockerfile. Do not run manually.
```