import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { MessageService } from '../message.service';
import { environment } from '../../environments/environment';
import { User } from '../user';

@Injectable()
export class AuthService {

  static readonly EGOVPASSPORT_USER: string = "egovpassport_user";

  private authLoginUrl = environment.restBaseUrl + "/auth/login";

  constructor(
    private http: HttpClient,
    private messageService: MessageService
  ) { }

  /**
   * Performs the login. Inspired on https://blog.angular-university.io/angular-jwt-authentication/
   * @param username 
   * @param password in clear text
   * @returns Observable<{ token: string }>
   */
  login(username: string, password: string | undefined, callback: (err: any, data?: User) => void): void {
    // backend /auth/login returns token
    const self = this;
    //self.log(`POST ${this.authLoginUrl}, ${username}, ${password}`);
    this.http.post<User>(this.authLoginUrl, { username, password })
      .subscribe({
        next(res: User) {
          //self.log(`posted ${username},${password}, ${res}`);
          if (!res.token || !res.email) {
            callback("Missing email/token field in " + JSON.stringify(res));
            return;
          }
          self.setSession(res);
          callback(null, res);
        },
        error(err: any) {
          callback(err);
        }
      });
  }

  private setSession(authResult: User): void {
    sessionStorage.setItem(AuthService.EGOVPASSPORT_USER, JSON.stringify(authResult));
  }

  public logout() {
    sessionStorage.clear();
  }

  public hasAdminProfile(): boolean {
    return this.isLoggedIn() && this.hasAnyProfile(); // TODO implement user profiles
  }

  public hasAnyProfile(): boolean {
    return this.isLoggedIn();
  }

  public isLoggedIn(): boolean {
    return !!sessionStorage.getItem(AuthService.EGOVPASSPORT_USER);
  }

  public isLoggedOut(): boolean {
    return !this.isLoggedIn();
  }

  public getToken(): string | undefined {
    return this.getUser()?.token;
  }

  public getUser(): User | undefined {
    if (this.isLoggedIn()) {
      return JSON.parse(sessionStorage.getItem(AuthService.EGOVPASSPORT_USER)!);
    } else {
      return undefined;
    }
  }

  public getUsername(): string | undefined {
    return this.getUser()?.email;
  }

  public getFullUserName(): string | undefined {
    return this.getUser()?.email;
  }

  private log(message: string) {
    console.log(message);
    this.messageService.add(`AuthService: ${message}`);
  }
}
