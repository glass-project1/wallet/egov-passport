
/**
 * Frontend user session object - should match reploy of /borest/auth/login
 */
export class User {
    userId: string | undefined; // UUID
    email: string | undefined; // used as username
    token: string | undefined; // backoffice-backend JWT token
}
