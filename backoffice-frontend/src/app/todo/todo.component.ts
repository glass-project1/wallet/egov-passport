import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common'
import {AppComponent} from "../app.component";

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TodoComponent implements OnInit {

  constructor(
    private appComponent: AppComponent,
    private location: Location
  ) { }

  ngOnInit(): void {
    this.appComponent.setNavMenuHighlight('data', 'For future development');
  }

  navigateBack(): void {
    this.location.back();
  }
}
