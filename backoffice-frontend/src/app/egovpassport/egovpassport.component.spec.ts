import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EGovPassportComponent } from './egovpassport.component';

describe('EGovPassportComponent', () => {
  let component: EGovPassportComponent;
  let fixture: ComponentFixture<EGovPassportComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EGovPassportComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EGovPassportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
