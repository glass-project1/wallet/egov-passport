import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { ENTER } from '@angular/cdk/keycodes';
import { FormArray, FormBuilder } from '@angular/forms';
import { first } from 'rxjs/operators';
import { MatChipInputEvent } from '@angular/material/chips';
import { MatTableDataSource } from '@angular/material/table';
import { MatSort, Sort, SortDirection } from '@angular/material/sort';
import { MatAutocompleteSelectedEvent } from '@angular/material/autocomplete';
import { PageEvent } from '@angular/material/paginator';

import { AppComponent } from '../app.component';
import { EGovPassportService } from '../egovpassport.service';
import { LocalStorageService } from '../localstorage.service';
import { EGovPassport } from '../egovpassport';

@Component({
  selector: 'app-egovpassport',
  templateUrl: './egovpassport.component.html',
  styleUrls: ['./egovpassport.component.css']
})
export class EGovPassportComponent implements OnInit {

  dataSource: MatTableDataSource<EGovPassport> = new MatTableDataSource();
  @ViewChild(MatSort) sort: MatSort = new MatSort();

  /** chipsInputFilters are inputs that accept multiple values as input, used in filter form */
  chipsInputs: ChipInputFilter[] = [
    { label: 'ID', name: 'id', elements: [] },
    { label: 'Passport Number', name: 'passportNumber', elements: [] },
    { label: 'Given Names', name: 'givenName', elements: [] },
    { label: 'Surnames', name: 'surname', elements: [] },
  ];

  tableDefaultColumns: CheckBox[] = [
    { label: 'ID', key: 'id', visible: true },
    { label: 'Passport Number', key: 'passportNumber', visible: true },
    { label: 'Given Names', key: 'givenName', visible: true },
    { label: 'Surnames', key: 'surname', visible: true },
  ];

  tableCustomColumns: CheckBox[] = [
    //{ label: 'Given Names', key: 'givenName', visible: false },
    //{ label: 'Surnames', key: 'surname', visible: false },
    { label: 'Issuing Date', key: 'issuingDate', visible: true },
    { label: 'Birth Date', key: 'birthDate', visible: true },
    { label: 'Wallet Key SSI', key: 'walletKeySSI', visible: true },
  ];

  public viewer: Viewer = {
    customColumnSelector: [],
    pageSizeOptions: [5, 10, 25, 50, 100, 500, 1000],
    showFirstLastButtons: true,
    tableDataRetriever: {
      id: {
        label: 'ID',
        data: (eGovPassport: EGovPassport) => eGovPassport.id,
      },
      passportNumber: {
        label: 'Passport Number',
        data: (eGovPassport: EGovPassport) => eGovPassport.passportNumber,
      },
      givenName: {
        label: 'Given Names',
        data: (eGovPassport: EGovPassport) => eGovPassport.givenName,
      },
      surname: {
        label: 'Surnames',
        data: (eGovPassport: EGovPassport) => eGovPassport.surname,
      },
      issuingDate: {
        label: 'Issuing Date',
        data: (eGovPassport: EGovPassport) => this.datePrettify(eGovPassport.issuingDate),
      },
      birthDate: {
        label: 'Birth Date',
        data: (eGovPassport: EGovPassport) => this.datePrettify(eGovPassport.birthDate),
      },
      walletKeySSI: {
        label: 'Wallet Key SSI',
        data: (eGovPassport: EGovPassport) => eGovPassport.walletKeySSI,
      },
    },
    chipsConfig: {
      visible: true,
      selectable: true,
      removable: true,
      addOnBlur: true,
      separatorKeysCodes: [ENTER],
    }
  };

  public handler: Handler = {
    displayedColumns: [],
    filterPanelOpen: false,
    isLoadingResults: true,
    tableManager: {
      pageIndex: 0,
      pageSize: 5,
      itemsCount: 10,
      issuingDateStart: '',
      issuingDateEnd: '',
      birthDateStart: '',
      birthDateEnd: '',
      sortProperty: 'id',
      sortDirection: 'asc',
      multipleInputFilters: this.formBuilder.array(this.chipsInputs),
      defaultColumns: this.formBuilder.array(this.tableDefaultColumns),
      customColumns: this.formBuilder.array(this.tableCustomColumns)
    }
  };

  tableManagerForm = this.formBuilder.group(this.handler.tableManager);

  constructor(
    private appComponent: AppComponent,
    private eGovPassportService: EGovPassportService,
    private formBuilder: FormBuilder,
    private ngZone: NgZone,
    private localStorageService: LocalStorageService) {
  }

  ngOnInit(): void {
    this.appComponent.setNavMenuHighlight('data', 'egovpassport', 'List of IDs+Passport');
    caches.has(LocalStorageService.EVENT_PAGE).then(() => {
      console.log('event.component.ngOnInit @tableManager =', this.tableManager);
      this.tableManager = this.localStorage;
      console.log('event.component.ngOnInit @tableManager.cache', this.tableManager);
    }).finally(() => {
      this.getEGovPassports(this.tableManager.pageSize, this.tableManager.pageIndex);
    });
  }


  /**
   * Prettify a javascript date to human format
   * @param date in format: YYYY-MM-dd HH-mm-ss
   */
  datePrettify(date: Date): string {
    const d = new Date(date);
    const day = d.getDate().toString().padStart(2, '0');
    const month = (d.getMonth() + 1).toString().padStart(2, '0');
    const year = d.getFullYear();
    /*
    const h = d.getHours().toString().padStart(2, '0');
    const m = d.getMinutes().toString().padStart(2, '0');
    const s = d.getSeconds().toString().padStart(2, '0');
    return `${year}-${month}-${day} ${h}:${m}:${s}`;
    */
    return `${year}-${month}-${day}`;
  }

  /** Perform API Request and made EventTableData interface
   * @param pageIndex Number of page
   * @param pageSize Number of records in each page
   */
  getEGovPassports(pageSize: number, pageIndex: number): void {
    this.handler.isLoadingResults = true;
    const filters: any[] = [
      {name: 'page', value: pageIndex},
      {name: 'limit', value: pageSize},
    ];

    const acceptFilterForm = ['issuingDateStart', 'issuingDateEnd', 'birthDateStart', 'birthDateEnd', 'sortDirection', 'sortProperty'];
    for (const [name, value] of Object.entries(this.tableManager)) {
      if (acceptFilterForm.includes(name) && !!value) {
        filters.push({
          name,
          value
        });
      }
    }

    this.multipleInputFilters.controls.forEach(chipInput => {
      const input = chipInput.value;
      if (input.elements.length > 0) {
        filters.push({
          name: input.name,
          value: input.elements
        });
      }
    });

    //console.log("Defcols", this.tableManagerDefaultColumns.value);
    //console.log("Custcols", this.tableManagerCustomColumns.value);
    const colsToDisplay : string[] = [];
    const cols = this.tableManagerDefaultColumns.value.concat(this.tableManagerCustomColumns.value);
    cols.forEach((column: CheckBox) => {
      if (column.visible) {
        //console.log("DISPLAY ", column);
        colsToDisplay.push(column.key);
      }
    });

    console.log('egovpassport.component.getEGovPassports filters=', filters);
    this.eGovPassportService.getAll(filters).subscribe((resp) => {

      this.handler.displayedColumns = colsToDisplay;
      console.log('event.component.getEGovPassports displayedColumns =', this.handler.displayedColumns);

      this.tableManager = {
        pageIndex: pageIndex,
        pageSize,
        itemsCount: resp.count,
        sortDirection: this.tableManager.sortDirection,
        sortProperty: this.tableManager.sortProperty
      };

      this.dataSource = new MatTableDataSource(resp.results);
      this.dataSource.sortingDataAccessor = (event, property) => {
        return this.tableDataRetriever[property].data(event);
      };
      this.dataSource.sort = this.sort;

      this.ngZone.onStable.pipe(first()).subscribe(() => {
        this.handler.isLoadingResults = false;
      });

    });
  }


  /** Handle the change of checkboxes in the selection of CUSTOM columns
   * @param event - object with checkBox status and input element from html DOM
   * @param checkBox -
   */
   handleCustomColumnSelector(event: any, checkBox: any): void {
    const {checked} = event;
    checkBox.value.visible = checked;
    console.log(
      'event.component.handleCustomColumnSelector',
      '@column =', checkBox.value.key,
      '@visible =', checkBox.value.visible,
      '@values =', this.tableManagerCustomColumns.value
    );
  }

  /** Handle the change of checkboxes in the selection of DEFAULT columns
   * * @param event - object with checkBox status and input element from html DOM
   * @param checkBox -
   */
  handleDefaultColumnSelector(event: any, checkBox: any): void {
    const {checked} = event;
    checkBox.value.visible = checked;
    console.log(
      'event.component.handleDefaultColumnSelector',
      '@column =', checkBox.value.key,
      '@visible =', checkBox.value.visible,
      '@values =', this.tableManagerDefaultColumns.value
    );
  }

  handleDownloadScans(): void {
    const emptyA : any = [];
    this.eGovPassportService.get(emptyA).subscribe((res: any) => {
      const fileName = this.datePrettify(new Date());
      this.downloadByHtmlElement(`${fileName}-egovPassport`, JSON.stringify(res));
    });
  }

  /** Create html tag to emit download event */
  private downloadByHtmlElement(fileName: string, content: string): void {
    const htmlElement = document.createElement('a');
    const fileType = 'text/plain';
    htmlElement.setAttribute(
      'href',
      `data:${fileType};charset=utf-8,${encodeURIComponent(content)}`
    );
    htmlElement.setAttribute('download', fileName);
    const event = new MouseEvent('click');
    htmlElement.dispatchEvent(event);
  }

  /** Listen actions in pagination component and do an action
   * @param pageEvent event metadata capture
   */
  handlePageEvent(pageEvent: PageEvent): void {
    console.warn('egovpassport.component.handlePageEvent pageEvent=', pageEvent);
    this.getEGovPassports(pageEvent.pageSize, pageEvent.pageIndex);
  }

  /** Listen filterForm/tableManager, when form data is submitted, data is retrieved from the form by property "value" */
  handleFilter(): void {
    console.warn('egovpassport.component.handleFilter formSubmitted=', this.tableManager);
    this.handler.filterPanelOpen = false;
    this.getEGovPassports(this.tableManager.pageSize, 0);
  }

  /** Handle add user inputs in chip inputs
   * @param chipInputFilter - object metadata to identifier the chipInput
   * @param event - object input element from html DOM
   */
  handleAddChip(chipInputFilter: any, event: MatChipInputEvent | MatAutocompleteSelectedEvent): void {
    console.log('chips.add -> chipInputFilter=', chipInputFilter);
    console.log('chips.add -> event=', event);
    let value: string;
    if ((event instanceof MatAutocompleteSelectedEvent)) {
      value = event.option.viewValue;
    } else {
      value = (event.value || '').trim();
      //event.input.value = '';
      if (event.chipInput)
        event.chipInput!.inputElement.value = '';
    }

    if (!!value && chipInputFilter.value.elements.indexOf(value) < 0) {
      chipInputFilter.value.elements.push(value);
    }
    console.log('event.component.handleAddChip chipsInputFilters=', this.chipsInputs);
  }


  /** Handle remove inputs in chip inputs
   * @param chipInputFilter - object metadata to identifier the chipInput
   * @param element - user input data (string)
   */
   handleRemoveChip(chipInputFilter: any, element: any): void {
    console.log('chips.remove -> chipInputFilter=', chipInputFilter);
    console.log('chips.remove -> element=', element);
    chipInputFilter.value.elements = chipInputFilter.value.elements.filter((r: any) => r !== element);
    console.log('event.component.handleRemoveChip chipsInputFilters=', this.tableManager.multipleInputFilters);
  }

  /**
   * Handle the sort event columns in the event table
   * @param event - sort object changed
   */
   handleSortData(event: Sort): void {
    const {active, direction} = event;
    this.tableManager.sortDirection = direction;
    this.tableManager.sortProperty = active;
    this.getEGovPassports(this.tableManager.pageSize, 0);
  }

  get localStorage(): any {
    return this.localStorageService.get(LocalStorageService.EVENT_PAGE) || {};
  }
  get multipleInputFilters(): FormArray {
    return this.tableManagerForm.get('multipleInputFilters') as FormArray;
  }

  get tableDataRetriever(): TableDataRetriever {
    return this.viewer.tableDataRetriever;
  }

  get tableManager(): TableManager {
    return this.tableManagerForm.value;
  }

  set tableManager(value: TableManager) {
    console.log('set data handler @valueReceived', value);
    this.tableManagerForm.patchValue(value, { emitEvent: false });
    this.localStorageService.set(LocalStorageService.EVENT_PAGE, this.tableManagerForm.value);
    console.log('get data handler @cache =', this.localStorage);
  }

  get tableManagerDefaultColumns(): FormArray {
    return this.tableManagerForm.get('defaultColumns') as FormArray;
  }

  get tableManagerCustomColumns(): FormArray {
    return this.tableManagerForm.get('customColumns') as FormArray;
  }
}


interface CustomInput {
  label: string;
  value: string;
}

interface TableDataRetriever {
  [key: string]: {
    label: string,
    data: any,
    cssClasses?: any
  };
}

interface ChipConfig {
  visible: boolean;
  selectable: boolean;
  removable: boolean;
  addOnBlur: boolean;
  readonly separatorKeysCodes: number[];
}

interface Viewer {
  customColumnSelector: string[];
  pageSizeOptions: number[];
  showFirstLastButtons: boolean;
  tableDataRetriever: TableDataRetriever;
  chipsConfig: ChipConfig;
}

interface ChipInputFilter {
  label: string;
  name: string;
  elements: string[];
  autocompleteOptions?: string[];
}

interface CheckBox {
  label: string;
  key: string;
  visible: boolean;
}

interface TableManager {
  pageIndex: number;
  pageSize: number;
  itemsCount: number;
  issuingDateStart?: string;
  issuingDateEnd?: string;
  birthDateStart?: string;
  birthDateEnd?: string;
  sortProperty: string;
  sortDirection: SortDirection;
  multipleInputFilters?: any;
  defaultColumns?: any;
  customColumns?: any;
}

interface Handler {
  displayedColumns: string[];
  filterPanelOpen: boolean;
  isLoadingResults: boolean;
  tableManager: TableManager;
}

