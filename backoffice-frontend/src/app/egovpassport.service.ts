import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpParams } from '@angular/common/http';

import { environment } from '../environments/environment';
import { MessageService } from './message.service';
import { PaginatedDto } from './paginated.dto';
import { EGovPassportQuery } from './egovpassport.query';
import { EGovPassport } from './egovpassport';

@Injectable({
  providedIn: 'root'
})
export class EGovPassportService {

  private borestBaseEGovPassportUrl = environment.restBaseUrl + "/egovpassport/egovpassport"; // + Id 

  constructor(private http: HttpClient, private messageService: MessageService) {
  }

  /** Perform API Request to create a wallet for one EGovId */
  createWallet(eGovPassportId: string): Observable<EGovPassport> {
    const url = this.borestBaseEGovPassportUrl + "/" + encodeURIComponent(eGovPassportId);
    console.log("Url: " + url);
    return this.http.post<EGovPassport>(url, null)
      .pipe(
        tap(_ => console.log(`created wallet for EGovPassport`)),
        catchError(this.handleError<EGovPassport>(`POST egovpassport/${eGovPassportId}`))
      );
  }

  // from https://stackoverflow.com/questions/41993515/access-object-key-using-variable-in-typescript
  typedKeys<T>(o: T): (keyof T)[] {
    // type cast should be safe because that's what really Object.keys() does
    return Object.keys(o) as (keyof T)[];
  }

  /** Perform API Request and get a list of all events */
  //getAll(eGovPassportQuery: EGovPassportQuery): Observable<PaginatedDto<EGovPassportQuery, EGovPassport>> {
  getAll(eGovPassportQuery: { name: string; value: string; }[] ): Observable<PaginatedDto<EGovPassportQuery, EGovPassport>> {
    let url = this.borestBaseEGovPassportUrl;
    let firstParam = true;
    if (eGovPassportQuery && eGovPassportQuery.length) { // encode query parameters in the URL
      eGovPassportQuery.forEach( (eGovPassportQueryElement) => {
        const name = eGovPassportQueryElement.name;
        const value = eGovPassportQueryElement.value;
        if (value) {
          if (firstParam) {
            url += '?';
            firstParam = false;
          } else {
            url += "&";
          }
          url += name + "=" + encodeURIComponent(value);
        }
      });
    }
    console.log("Url: " + url);

    return this.http.get<PaginatedDto<EGovPassportQuery, EGovPassport>>(url).pipe(
      tap(_ => this.log(`fetched EGovPassports`)),
      catchError(this.handleError<any>('getAll'))
    );
  }

  /** Perform API Request and get one EGovPassport */
  get(eGovPassportId: string): Observable<EGovPassport> {
    const url = this.borestBaseEGovPassportUrl + "/" + encodeURIComponent(eGovPassportId);
    console.log("Url: " + url);
    return this.http.get<EGovPassport>(url)
      .pipe(
        tap(_ => console.log(`fetched EGovId`)),
        catchError(this.handleError<EGovPassport>(`GET egovpassport/${eGovPassportId}`))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: any): any {
    return (error: any): Observable<T> => {
      console.error(error); // log to console instead
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  private log(message: string): void {
    this.messageService.add(`EventService: ${message}`);
  }

}
