export interface EGovPassport {

    id: string;

    passportNumber: string;

    givenName: string;

    surname: string;

    issuingDate: Date;

    expirationDate: Date;

    issuingLocation: string;

    issuingAuthority: string;

    birthDate: Date;

    birthLocation: string;

    nationality: string;

    gender: string;

    height: string;

    walletKeySSI: string;

    country: string; // not used - but may be used in multi-country setups
}
