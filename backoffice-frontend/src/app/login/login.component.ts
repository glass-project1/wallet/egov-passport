import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Validators } from '@angular/forms';

import { MessageService } from '../message.service';
import { AuthService } from '../auth/auth.service';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = this.formBuilder.group({
    username: ['', Validators.required],
    password: ['']
  });

  errorMessage = "";
  loading = false;

  constructor(
    private appComponent: AppComponent,
    private formBuilder: FormBuilder,
    private router: Router,
    private authService: AuthService,
    private messageService: MessageService
  ) {
  }

  ngOnInit(): void {
    this.appComponent.logout();
    this.appComponent.setNavMenuHighlight("", "login", "GLASS egovPassport Legacy DEMO Login");
    this.loginForm.reset();
    this.errorMessage = "";
    if (this.authService.isLoggedIn())
      this.router.navigate(['/egovpassport']); // TODO navigate to proper profile entry page
  }

  async login() {
    let self = this;
    self.loading = true;
    self.errorMessage = "";
    if (!self.loginForm.value.username) {
      self.log("Username cannot be empty!");
      return;
    }
    let auUsername = self.loginForm.value.username;
    let auPassword = self.loginForm.value.password;
    self.loginForm.controls['password'].setValue(""); // clear the password anyway
    self.messageService.add("Logging in \"" + auUsername + "\"");
    await new Promise(wait => setTimeout(wait, 980));
    self.authService.login(auUsername, auPassword,
      function (err, res) {
        if (err) {
          self.log("Logged in \"" + auUsername + "\" failed " + JSON.stringify(err));
          if (err?.status == 401) { // HTTP status Unauthorized
            self.errorMessage = "Wrong username or password!";
          } else {
            self.errorMessage = "Weird error!";
          }
        } else {
          self.log("Logged in " + auUsername + " res=" + JSON.stringify(res));
          self.router.navigate(['/egovpassport']); // TODO navigate to proper profile entry page
        }
      }
    );
    self.loading = false;
  }

  get username() {
    return this.loginForm.get('username');
  }

  private log(message: string) {
    this.messageService.add(`LoginComponent: ${message}`);
  }
}
