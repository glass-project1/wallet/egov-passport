export class EGovPassportQuery {

    id: string | undefined;

    passportNumber: string | undefined;

    givenName: string | undefined;

    surname: string | undefined;

    issueDateStart: string | undefined;
    issueDateEnd: string | undefined;

    birthDateStart: string | undefined;
    birthDateEnd: string | undefined;
    
    // common to all queries

    limit: number | undefined;

    page: number | undefined;

    sortProperty: string | undefined;

    sortDirection: string | undefined;
}
