import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppResourceDetailComponent } from './appresource-detail/appresource-detail.component';
import { AppResourceComponent } from './appresource/appresource.component';
import { EGovPassportDetailComponent } from './egovpassport-detail/egovpassport-detail.component';
import { EGovPassportComponent } from './egovpassport/egovpassport.component';
import { LoginComponent } from './login/login.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { TodoComponent } from './todo/todo.component';

const routes: Routes = [
  { path: 'appresource', component: AppResourceComponent },
  { path: 'appresource/:id', component: AppResourceDetailComponent },
  { path: 'egovpassport', component: EGovPassportComponent },
  { path: 'egovpassport/:id', component: EGovPassportDetailComponent },
  { path: 'login', component: LoginComponent },
  { path: 'home', redirectTo: '/egovpassport' },
  { path: 'todo', component: TodoComponent },
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
