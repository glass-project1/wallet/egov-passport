import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BwipComponent } from './bwip.component';

describe('BwipComponent', () => {
  let component: BwipComponent;
  let fixture: ComponentFixture<BwipComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BwipComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BwipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
