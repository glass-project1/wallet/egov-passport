import { Component, ViewChild, ElementRef, OnChanges, OnInit, Input } from '@angular/core';
import bwipjs from 'bwip-js'; // tsconfig.js has "allowSyntheticDefaultImports": true because of this :-(
// import bwipjs = require('bwip-js');

@Component({
  selector: 'bwip-component',
  template: '<canvas #barcodeElement></canvas>',
  styleUrls: []
})
export class BwipComponent implements OnChanges, OnInit {

  @ViewChild('barcodeElement', { static: true }) barcodeElement: ElementRef | undefined;

  @Input() textValue : string = "?";

  constructor() { }

  ngOnInit(): void {
    try {
      console.log("ngOnInit", this.barcodeElement);
      // The return value is the canvas element
      const canvas = bwipjs.toCanvas(this.barcodeElement?.nativeElement, {
        bcid: 'qrcode',
        text: this.textValue,
        //bcid:        'code128',       // Barcode type
        //text:        '0123456789',    // Text to encode
        //scale:       3,               // 3x scaling factor
        //height:      10,              // Bar height, in millimeters
        includetext: true,            // Show human-readable text
        textxalign:  'center',        // Always good to set this
      });
    }
    catch (e) {
      // 'e' may be a string or Error object
      console.log(e);
    }
  }

  ngOnChanges() {
    try {
      console.log("ngOnChanges", this.barcodeElement);
      // The return value is the canvas element
      const canvas = bwipjs.toCanvas(this.barcodeElement?.nativeElement, {
        bcid: 'qrcode',
        text: this.textValue,
        //bcid:        'code128',       // Barcode type
        //text:        '0123456789',    // Text to encode
        //scale:       3,               // 3x scaling factor
        //height:      10,              // Bar height, in millimeters
        includetext: true,            // Show human-readable text
        textxalign:  'center',        // Always good to set this
      });
    }
    catch (e) {
      // 'e' may be a string or Error object
      console.log(e);
    }
  }

}
