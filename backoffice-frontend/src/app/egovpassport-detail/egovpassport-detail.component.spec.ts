import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EGovPassportDetailComponent } from './egovpassport-detail.component';

describe('EGovPassportDetailComponent', () => {
  let component: EGovPassportDetailComponent;
  let fixture: ComponentFixture<EGovPassportDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EGovPassportDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EGovPassportDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
