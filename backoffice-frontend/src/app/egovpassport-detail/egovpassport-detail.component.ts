import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EGovPassport } from '../egovpassport';
import { EGovPassportService } from '../egovpassport.service';

@Component({
  selector: 'app-egovpassport-detail',
  templateUrl: './egovpassport-detail.component.html',
  styleUrls: ['./egovpassport-detail.component.css']
})
export class EGovPassportDetailComponent implements OnInit {

  eGovPassport: any = { id: '?', givenName: '?', birthDate: new Date() };

  showWalletQrCode : boolean = false;

  constructor(
    private eGovPassportService: EGovPassportService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.showWalletQrCode = false;
    const eGovPassportId = this.route.snapshot.paramMap.get('id');
    if (!eGovPassportId) {
      console.log("No id parameter");
      return;
    }
    this.eGovPassportService.get(eGovPassportId)
      .subscribe(eGovPassport => {
        this.eGovPassport = eGovPassport;
      });
  }

  createWallet() {
    const self = this;
    self.eGovPassportService.createWallet(self.eGovPassport.id)
      .subscribe(eGovPassport => {
        this.eGovPassport = eGovPassport;
      });
  }

  /*
  editCitizenship() {
    this.router.navigateByUrl("/egovpassport-citizenship-edit/"+this.eGovPassport.id);
  }
  */

  toogleShowWalletQrCode() {
    this.showWalletQrCode = !this.showWalletQrCode;
  }

  navigateBack() {
    this.router.navigateByUrl("/egovpassport");
  }
}
